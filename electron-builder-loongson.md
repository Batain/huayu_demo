electron-builder打包增加mips64el架构随笔
===
## 1. 源码下载路径

[electron-builder@20.38.2](https://github.com/electron-userland/electron-builder/archive/v20.38.2.zip)

## 2. 修改文件路径

> electron-builder-20.38.2/packages/app-builder-lib/src

### 2.1 修改的文件

1. ./linuxPackager.ts:121

``` java
export function toAppImageOrSnapArch(arch: Arch): string {
   switch (arch) {
     case Arch.x64:
       return "x86_64"
     case Arch.ia32:
       return "i386"
     case Arch.armv7l:
       return "arm"
     case Arch.arm64:
       return "arm_aarch64"
     case Arch.mips64el:
       return "mips64el"

     default:
       throw new Error(`Unsupported arch ${arch}`)
   }
}
```

2. ./targets/MsiTarget.ts:78

``` java
const candleArgs = [
       "-arch", arch === Arch.ia32 ? "x86" : (arch === Arch.armv7l ? "arm" : (arch === Arch.mips64el ? "mips64el" : "X64")),
       `-dappDir=${vm.toVmFile(appOutDir)}`,
    ].concat(this.getCommonWixArgs())
```

## 3. 修改文件路径

> electron-builder-20.38.2/packages/electron-builder/src

### 3.1 修改的文件

3.1.1. ./builder.ts:15

``` java
 10 export interface CliOptions extends PackagerOptions, PublishOptions {
 11   arch?: string
 12
 13   x64?: boolean
 14   ia32?: boolean
 15   armv7l?: boolean
 16   arm64?: boolean
 17   mips64el?: boolean
 18
 19   dir?: boolean
 20
 21   platform?: string
 22 }
---------------------------------------
 41       function commonArch(currentIfNotSpecified: boolean): Array<Arch> {
 42       if (platform === Platform.MAC) {
 43         return args.x64 || currentIfNotSpecified ? [Arch.x64] : []
 44       }
 45
 46       const result = Array<Arch>()
 47       if (args.x64) {
 48         result.push(Arch.x64)
 49       }
 50       if (args.armv7l) {
 51         result.push(Arch.armv7l)
 52       }
 53       if (args.arm64) {
 54         result.push(Arch.arm64)
 55       }
 56       if (args.ia32) {
 57         result.push(Arch.ia32)
 58       }
 59
 60       return result.length === 0 && currentIfNotSpecified ?[archFromString(process.arch)] : result
 61     }

----------------------------------
143   delete result.ia32
144   delete result.x64
145   delete result.armv7l
146   delete result.arm64
147   delete result.mips64el
-----------------------------------
270     .option("armv7l", {
271       group: buildGroup,
272       description: "Build for armv7l",
273       type: "boolean",
274     })
275     .option("arm64", {
276       group: buildGroup,
277       description: "Build for arm64",
278       type: "boolean",
279     })
280     .option("mips64el", {
281       group: buildGroup,
282       description: "Build for mips64el",
283       type: "boolean",
284     })
```

3.1.2. ./cli/install-app-deps.ts

``` java
 27     .option("arch", {
 28       choices: getArchCliNames().concat("all"),
 29 //      default: process.arch === "arm" ? "armv7l" : process.arch,
 30       default: process.arch === "arm" ? "armv7l" : (process.arch === "mips64el" ? "mips64el" : process.arch),
 31       description: "The target arch",
 32     })
 33 }

```

## 4. 修改文件路径

> electron-builder-20.38.2/packages/builder-util/src

### 4.1 修改的文件

4.1.1 ./arch.ts

``` java
  7 export function toLinuxArchString(arch: Arch): string {
  8   switch (arch) {
  9     case Arch.x64:
 10       return "amd64"
 11     case Arch.ia32:
 12       return "i386"
 13     case Arch.armv7l:
 14       return "armv7l"
 15     case Arch.arm64:
 16       return "arm64"
 17     case Arch.mips64el:
 18       return "mips64el"
 19
 20     default:
 21       throw new Error(`Unsupported arch ${arch}`)
 22   }
 23 }
-------------------------------------------------------

 25 export function getArchCliNames(): Array<string> {
 26   return [Arch[Arch.ia32], Arch[Arch.x64], Arch[Arch.armv7l], Arch[Arch.arm64], Arch[Arch.mips64el]]
 27 }
--------------------------------------------------------

 33 export function archFromString(name: string): Arch {
 34   switch (name) {
 35     case "x64":
 36       return Arch.x64
 37     case "ia32":
 38       return Arch.ia32
 39     case "arm64":
 40       return Arch.arm64
 41     case "armv7l":
 42       return Arch.armv7l
 43     case "mips64el":
 44       return Arch.mips64el
 45
 46     default:
 47       throw new Error(`Unsupported arch ${name}`)
 48   }
-------------------------------------------------------
```

## 5. 将修改过的源代码，重新编译

### 5.1 进入electron-builder-20.38.2目录,执行以下操作

``` shell
cd electron-builder-20.38.2
npm install
npm install compile-run
npm install ts-babel
npm install yarns
npm install dts-gen
npm install yarn
```

### 5.2 进行重新编译

```shell
 npm run compile
```

#### 5.2.1在执行过程中报错，报错信息如下：

> @ compile /home/dyh/tmp/electron-builder-20.38.2
>
> ts-babel "packages/*" test && yarn schema
>
>Building builder-util-runtime
>
>Building builder-util, electron-updater
>
>/home/dyh/tmp/electron-builder-20.38.2/packages/builder-util/src/util.ts (194, 5): Object is possibly 'null'.
>npm ERR! code ELIFECYCLE
>
>npm ERR! errno 255
>
>npm ERR! @ compile: `ts-babel "packages/*" test && yarn schema`
>
>npm ERR! Exit status 255
>
>npm ERR!
>
>npm ERR! Failed at the @ compile script.
>
>npm ERR! This is probably not a problem with npm. There is likely 
additional logging output above.

#### 5.2.2 解决办法:

``` shell
./node_modules/.bin/yarn
npm run compile
```

---

 打包过程中遇到的问题解决
===

## 1. 报错信息

> OKAY  take it away `electron-builder`
>
> • electron-builder version=20.38.2
>
> • loaded configuration file=package.json ("build" field)
>
> • writing effective config file=build/builder-effective-config.yaml
>
> <font color='red'>**Error: Unsupported arch mips64el** </font>
>
> at archFromString (/home/dyh/huayu/tmp/my-project/node_modules/builder-util/src/arch.ts:40:10)
>
> at computeArchToTargetNamesMap (/home/dyh/huayu/tmp/my-project/node_modules/app-builder-lib/src/targets/targetFactory.ts:36:18)
>
> at /home/dyh/huayu/tmp/my-project/node_modules/app-builder-lib/src/packager.ts:415:41
>
> at Generator.next (<anonymous>)

### 1.1 解决方案

> 将上述执行npm run compile 重编译出来的 文件拷贝node_modules/builder-util/下，替换原来的out目录

```shell
cp -r ./packages/builder-util/out  /home/thtf/demo/node_modules/builder-util/
```

## 2. 报错信息

> OKAY  take it away `electron-builder`
>
> • electron-builder version=20.38.2
>
> • loaded configuration file=package.json ("build" field)
>
> • writing effective config file=build/builder-effective-config.yaml
>
> • no native production dependencies
>
> • packaging       platform=linux arch=mips64el electron=4.0.6 appOutDir=build/linux-mips64el-unpacked
>
> • building        target=snap arch=mips64el file=build/buildtest_0.0.1_mips64el.snap
>
> • building        target=AppImage arch=mips64el file=build/buildtest 0.0.1 mips64el.AppImage
>
> • application Linux category is set to default "Utility" reason=linux.category is not set and cannot map from macOS docs=https://electron.build/configuration/configuration#LinuxBuildOptions-category
>
> • default Electron icon is used reason=application icon is not set
>
> <font color='red'> **⨯ enum value must be one of x64,ia32,armv7l,arm64, got 'mips64el'** </font>
>
><font color='red'>
>
> Error: Cannot cleanup:
>
> Error #1 --------------------------------------------------------------------------------
>
> Error: Unsupported arch 4
>
> at toAppImageOrSnapArch (/home/dyh/tmp/my-project/node_modules/app-builder-lib/src/linuxPackager.ts:127:13)
>
> at SnapTarget.createDescriptor (/home/dyh/tmp/my-project/node_modules/app-builder-lib/src/targets/snap.ts:43:27)
>
> at /home/dyh/tmp/my-project/node_modules/app-builder-lib/src/targets/snap.ts:160:28 </font>
>

### 2.1 查找报错信息过程

> 通过对报错信息的全句搜索，发现报错该问题的问件来自/app-builder-bin/linux/ 下的app-builder这个二进制文件，发现没有对应mips64el架构的该二进制文件

### 2.2 解决方案

> 编译支持龙心平台的 *app-builder*

#### 2.2.1 **app-builder** 源码下载地址

[app-builder 源码](https://github.com/develar/app-builder)

> <font color='	DarkRed'>先使用 ***npm ls app-builder-bin*** 确认 app-builder 使用的版本 </font>
>
>  <font color='red'>本次使用的版本为 *app-builder-2.6.0*，在使用*app-builder-3.5.0* 时，能编译通过，但是在打包时会报错 </font>

#### 2.2.2 编译龙芯平台的 app-builder

1. go 环境需要 1.9.1 以上版本,需要从源码构建新的 go 版本。

2. go1.12编译

3. [go 高版本下载地址](https://studygolang.com/dl)
4. [go环境的编译](https://www.cnblogs.com/findumars/p/6794420.html)

#### 2.2.3 对app-builder-2.6.0源码添加mips64el架构的支持

1. 文件路径

> ./.goreleaser.yml:11

```go
    goarch:
      - 386
      - amd64
      - arm
      - arm64
      - mips64el
```

2. 文件路径

> ./scripts/publish-npm.sh:10

```go
mkdir -p app-builder-bin/linux
mkdir -p app-builder-bin/linux/ia32
mkdir -p app-builder-bin/linux/x64
mkdir -p app-builder-bin/linux/arm
mkdir -p app-builder-bin/linux/arm64
mkdir -p app-builder-bin/linux/mips64el
cp dist/app-builder_linux_386/app-builder app-builder-bin/linux/ia32/app-builder
cp dist/app-builder_linux_amd64/app-builder app-builder-bin/linux/x64/app-builder
cp dist/app-builder_linux_arm_7/app-builder app-builder-bin/linux/arm/app-builder
cp dist/app-builder_linux_arm64/app-builder app-builder-bin/linux/arm64/app-builder
cp dist/app-builder_linux_mips64el/app-builder app-builder-bin/linux/mips64el/app-builder
```

 2.1.  使用 *app-builder-3.5.0* 高版本后的报错信息

> OKAY  take it away `electron-builder`
>
> • electron-builder version=0.0.0-semantic-release
>
> • loaded configuration file=package.json ("build" field)
>
> • writing effective config file=build/builder-effective-config.yaml
>
>• no native production dependencies
>
> • packaging       platform=linux arch=mips64el electron=4.0.6  appOutDir=build/linux-mips64el-unpacked
> Error: ENOENT: no such file or directory, scandir '/home/dyh/tmp/demo/node_modules/[object Object]'
>
> at NodeModuleCopyHelper.collectNodeModules (/home/dyh/tmp/demo/node_modules/app-builder-lib/src/util/NodeModuleCopyHelper.ts:37:34)
>
> at _bluebirdLst.default.mapSeries (/home/dyh/tmp/demo/node_modules/app-builder-lib/src/util/appFileCopier.ts:212:32)

3. 文件路径
> ./scripts/publish.sh:30

```go
publish "linux_386" linux-ia32
publish "linux_amd64" linux-x64
publish "linux_arm_7" linux-armv7
publish "linux_arm64" linux-armv8
publish "linux_mips64el" linux-mips64el
```

4. 文件路径

> ./app-builder.iml

``` go
      <content url="file://$MODULE_DIR$">
      <excludeFolder url="file://$MODULE_DIR$/app-builder-bin/linux" />
      <excludeFolder url="file://$MODULE_DIR$/app-builder-bin/linux/arm" />
      <excludeFolder url="file://$MODULE_DIR$/app-builder-bin/linux/arm64" />
      <excludeFolder url="file://$MODULE_DIR$/app-builder-bin/linux/ia32" />
      <excludeFolder url="file://$MODULE_DIR$/app-builder-bin/linux/x64" />
      <excludeFolder url="file://$MODULE_DIR$/app-builder-bin/linux/mips64el" />
      <excludeFolder url="file://$MODULE_DIR$/app-builder-bin/win/ia32" />
      <excludeFolder url="file://$MODULE_DIR$/app-builder-bin/win/x64" />
```

5. 文件路径

> ./pkg/package-format/appimage/appImage.go:41

``` go
        options := &AppImageOptions{
                appDir:   command.Flag("app", "The app dir.").Short('a').Required().String(),
                stageDir: command.Flag("stage", "The stage dir.").Short('s').Required().String(),
                output:   command.Flag("output", "The output file.").Short('o').Required().String(),
                arch:     command.Flag("arch", "The arch.").Default("x64").Enum("x64", "ia32", "armv7l", "arm64", "mips64el")
        ......
                }
```

6. 文件路径

> ./pkg/package-format/snap/snap.go:108

``` shell
        func ConfigureCommand(app *kingpin.Application) {
        command := app.Command("snap", "Build snap.")

        templateFile := command.Flag("template", "The template file.").Short('t').String()

        templateUrl := command.Flag("template-url", "The template archive URL.").Short('u').String()
        templateSha512 := command.Flag("template-sha512", "The expected sha512 of template archive.").String()

        var isUseDockerDefault string
        if runtime.GOOS == "linux" || runtime.GOOS == "darwin" {
                isUseDockerDefault = "false"
        } else {
                isUseDockerDefault = "true"
        }

        //noinspection SpellCheckingInspection
        options := SnapOptions{
                appDir:         command.Flag("app", "The app dir.").Short('a').Required().String(),
                stageDir:       command.Flag("stage", "The stage dir.").Short('s').Required().String(),
                icon:           command.Flag("icon", "The path to the icon.").String(),
                hooksDir:       command.Flag("hooks", "The hooks dir.").String(),
                executableName: command.Flag("executable", "The executable file name to create command wrapper.").String(),

                arch: command.Flag("arch", "The arch.").Default("amd64").Enum("amd64", "i386", "armv7l", "arm64", "mips64el"),

                output: command.Flag("output", "The output file.").Short('o').Required().String(),

                dockerImage: command.Flag("docker-image", "The docker image.").Default("snapcore/snapcraft:latest").String(),
        }

```

7. 文件路径

> ./pkg/download/tool.go

``` go
func DownloadTool(descriptor ToolDescriptor, osName util.OsName) (string, error) {
        arch := runtime.GOARCH
        switch arch {
        case "arm":
                //noinspection SpellCheckingInspection
                arch = "armv7"
        case "arm64":
                //noinspection SpellCheckingInspection
                arch = "armv8"
        case "amd64":
                arch = "x64"
        case "mips64el":
                arch = "mips64el"
        }
```

8. 文件路径

> ./Makefile

```go

.PHONY: lint build publish assets

OS_ARCH = "mips64el"
ifeq ($(OS),Windows_NT)
        ifeq ($(PROCESSOR_ARCHITEW6432),AMD64)
                OS_ARCH := windows_amd64
        else
                OS_ARCH := windows_386
        endif
else
        UNAME_S := $(shell uname -s)
       ifeq ($(UNAME_S),Linux)
               OS_ARCH := linux_amd64
       endif
       ifeq ($(UNAME_S),Darwin)
               OS_ARCH := darwin_amd64
       endif
        ifeq ($(UNAME_S),Linux)
                OS_ARCH := linux_mips64el
         endif
         ifeq ($(UNAME_S),Darwin)
             OS_ARCH := darwin_mips64el
         endif

endif

```

## 3. 报错信息

> • default Electron icon is used reason=application icon is not set
>  
> • application Linux category is set to default "Utility" reason=linux.category is not set and cannot map from macOS docs=https://electron.build/configuration/configuration#LinuxBuildOptions-category
>  
> <font color='red'>**⨯ snapcraft is not installed, please: sudo snap install snapcraft --classic** </font>
>  
> • downloading               parts=1 size=1.5 MB url=https://github.com/electron-userland/electron-builder-binaries/releases/download/appimage-9.1.0/appimage-9.1.0.7z
>  
> • retrying (1)
>
> • downloaded                duration=3m3.649s url=https://github.com/electron-userland/electron-builder-binaries/releases/download/appimage-9.1.0/appimage-9.1.0.7z
>  
> <font color='red'> **⨯ fork/exec /home/dyh/tmp/my-project/node_modules/7zip-bin/linux/mips64el/7za: no such file or directory** </font>
>  
>github.com/develar/app-builder/pkg/download.DownloadArtifact   /home/thtf/huayu/package/app-builder-2.6.0/pkg/download/artifactDownloader.go:121

### 3.1 解决办法

将系统**/usr/bin/7za** 二进制文件拷贝到**node_modules/7zip-bin/linux/mips64el/7za**

```shell
cp /usr/bin/7za XXXXX/node_modules/7zip-bin/linux/mips64el/7za
```

修改文件**node_modules/app-builder-lib/out/linuxPacage.js**

```js
   get defaultTarget() {
     return ["rpm", "appimage"];
   }

```

## 4. 报错信息

> • downloading               parts=1 size=1.5 MB url=https://github.com/electron-userland/electron-builder-binaries/releases/download/appimage-9.1.0/appimage-9.1.0.7z
>  
> • downloaded                duration=5.051s url=https://github.com/electron-userland/electron-builder-binaries/releases/download/appimage-9.1.0/appimage-9.1.0.7z
>  
> <font color='red'> **⨯ open /home/dyh/.cache/electron-builder/appimage/appimage-9.1.0/runtime-mips64el: no such file or directory** </font>
>  
>github.com/develar/app-builder/pkg/package-format/appimage.AppImage

### 4.1 解决办法

> 重新编译appimage
>
> 编译appimage 需要安装cmake3.6.3以上版本

[AppImageKit源码](https://github.com/AppImage/AppImageKit)

[cmake 源码下载](https://github.com/Kitware/CMake/archive/v3.13.5.zip)

[fuse 源码下载](https://github.com/libfuse/libfuse/archive/fuse-2.9.8.zip)

#### 4.1.1 cmake 安装

``` shell
解压
tar -zxvf cmake-3.15.0-rc3.tar.gz


配置
cd cmake-3.13.5
./bootstrap --prefix=$PREFIX --parallel=4

编译安装：
make -j 4

安装：
make install

验证：
 cmake --version
cmake version 3.13.5
```

---

#### 4.1.2 Appimage源码修改

修改文件
> ./.travis.yml

```c
  7 matrix:
  8   include:
  9     - env: ARCH=x86_64 DOCKER_IMAGE=quay.io/appimage/appimagebuild
 10     - env: ARCH=i686 DOCKER_IMAGE=quay.io/appimage/appimagebuild-i386
 11     - env: ARCH=armhf DOCKER_IMAGE=quay.io/appimage/appimagebuild-armhf-cross
 12     - env: ARCH=mips64el DOCKER_IMAGE=quay.io/appimage/appimagebuild-mips64el-cross
 13       addons:

```

修改文件
> ./build-appdir.sh:43

```c
if [ -d /deps/ ]; then
    # deploy glib
    mkdir -p "$APPIMAGETOOL_APPDIR"/usr/lib/
    cp /deps/lib/lib*.so* "$APPIMAGETOOL_APPDIR"/usr/lib/
    # libffi is a runtime dynamic dependency
    # see this thread for more information on the topic:
    # https://mail.gnome.org/archives/gtk-devel-list/2012-July/msg00062.html
    if [ "$ARCH" == "x86_64" ]; then
        cp /usr/lib64/libffi.so.5 "$APPIMAGETOOL_APPDIR"/usr/lib/
    elif [ "$ARCH" == "i686" ]; then
        cp /usr/lib/libffi.so.5 "$APPIMAGETOOL_APPDIR"/usr/lib/
    elif [ "$ARCH" == "armhf" ] || [ "$ARCH" == "aarch64" ] || ["$ARCH" == "mips64el"]; then
        cp /deps/lib/libffi.so.6 "$APPIMAGETOOL_APPDIR"/usr/lib/
    else
        echo "WARNING: unknown architecture, not bundling libffi"
    fi
fi

```

修改文件

> ./src/appimagetool.c:70

```c
enum fARCH {
    fARCH_i386,
    fARCH_x86_64,
    fARCH_arm,
    fARCH_aarch64,
    fARCH_mips64el
};


----------------------------------------------------

 299 gchar* getArchName(bool* archs) {
 300     if (archs[fARCH_i386])
 301         return "i386";
 302     else if (archs[fARCH_x86_64])
 303         return "x86_64";
 304     else if (archs[fARCH_arm])
 305         return "armhf";
 306     else if (archs[fARCH_aarch64])
 307         return "aarch64";
 308     else if (archs[fARCH_mips64el])
 309         return "mips64el";
 310     else
 311         return "all";
 312 }

-------------------------------------------------------

314 void extract_arch_from_e_machine_field(int16_t e_machine, const gchar* sourcename, bool* archs) {
 315     if (e_machine == 3) {
 316         archs[fARCH_i386] = 1;
 317         if(verbose)
 318             fprintf(stderr, "%s used for determining architecture i386\n", sourcename);
 319     }
 320  
 321     if (e_machine == 62) {
 322         archs[fARCH_x86_64] = 1;
 323         if(verbose)
 324             fprintf(stderr, "%s used for determining architecture x86_64\n", sourcename);
 325     }
 326  
 327     if (e_machine == 40) {
 328         archs[fARCH_arm] = 1;
 329         if(verbose)
 330             fprintf(stderr, "%s used for determining architecture armhf\n", sourcename);
 331     }
 332  
 333     if (e_machine == 183) {
 334         archs[fARCH_aarch64] = 1;
 335         if(verbose)
 336             fprintf(stderr, "%s used for determining architecture aarch64\n", sourcename);
 337     }
 338  
 339     if (e_machine == 40) {
 340         archs[fARCH_mips64el] = 1;
 341         if(verbose)
 342             fprintf(stderr, "%s used for determining architecture mips64el\n", sourcename);
 343     }
 344 }

-------------------------------------------------------------


 void extract_arch_from_text(gchar *archname, const gchar* sourcename, bool* archs) {
 347     if (archname) {
 348         archname = g_strstrip(archname);
 349         if (archname) {
 350             replacestr(archname, "-", "_");
 351             replacestr(archname, " ", "_");
 352             if (g_ascii_strncasecmp("i386", archname, 20) == 0
 353                     || g_ascii_strncasecmp("i486", archname, 20) == 0
 354                     || g_ascii_strncasecmp("i586", archname, 20) == 0
 355                     || g_ascii_strncasecmp("i686", archname, 20) == 0
 356                     || g_ascii_strncasecmp("intel_80386", archname, 20) == 0
 357                     || g_ascii_strncasecmp("intel_80486", archname, 20) == 0
 358                     || g_ascii_strncasecmp("intel_80586", archname, 20) == 0
 359                     || g_ascii_strncasecmp("intel_80686", archname, 20) == 0
 360                     ) {
 361                 archs[fARCH_i386] = 1;
 362                 if (verbose)
 363                     fprintf(stderr, "%s used for determining architecture i386\n", sourcename);
 364             } else if (g_ascii_strncasecmp("x86_64", archname, 20) == 0) {
 365                 archs[fARCH_x86_64] = 1;
 366                 if (verbose)
 367                     fprintf(stderr, "%s used for determining architecture x86_64\n", sourcename);
 368             } else if (g_ascii_strncasecmp("arm", archname, 20) == 0) {
 369                 archs[fARCH_arm] = 1;
 370                 if (verbose)
 371                     fprintf(stderr, "%s used for determining architecture ARM\n", sourcename);
 372             } else if (g_ascii_strncasecmp("arm_aarch64", archname, 20) == 0) {
 373                 archs[fARCH_aarch64] = 1;
 374                 if (verbose)
 375                     fprintf(stderr, "%s used for determining architecture ARM aarch64\n", sourcename);
 376             }
 377             } else if (g_ascii_strncasecmp("mips64el", archname, 20) == 0) {
 378                 archs[fARCH_mips64el] = 1;
 379                 if (verbose)
 380                     fprintf(stderr, "%s used for determining architecture MIPS64\n", sourcename);
 381         }
 382     }
 383 }

```

1. 编译appimage

```shell

cmake -B build

--------------------------------------------------------
提示缺少 glib-2.0

解决办法:
yum install libgnomeui-devel

----------------------------------------------------------
 提示缺少 fuse

解决办法：重新编译安装fuse

cd libfuse-2.9.8
./makeconf.sh
./configure
make && make install

export PKG_CONFIG_PATH=&PKG_CONFIG_PTH:/usr/local/lib/pkgconfig


cmake -B build  编译成功
```

> 1. 将编译出来的**appruntime** 拷贝到 **/home/xxx/.cache/electron-builder/appimage/appimage-9.1.0** 目录下
>
> 2. 拷贝 *build/AppImageKit/build/mksquashfs-prefix/mksquashfs* 文件到 **/home/xxx/.cache/electron-builder/appimage/appimage-9.1.0/linux-mips64le** 目录下，**linux-mips64le**目录需要手动创建

```shell

cp /home/xxx/tmp/software/AppImageKit/build/src/runtime /home/xxx/.cache/electron-builder/appimage/appimage-9.1.0/runtime-mips64el

mkdir -p /home/xxx/.cache/electron-builder/appimage/appimage-9.1.0/linux-mips64le

cp /home/xxx/tmp/software/AppImageKit/build/mksquashfs-prefix/mksquashfs /home/xxx/.cache/electron-builder/appimage/appimage-9.1.0/linux-mips64el

```

> <font color='red'> **注意：这里的目录名是linux-mips64le**  </font>

## 5. 报错信息

> <font color='red'>   **• default Electron icon is used reason=application icon is not set
> Error: Please specify project homepage, see https://electron.build/configuration/configuration#Metadata-homepage 
> at FpmTarget.computeFpmMetaInfoOptions (/home/dyh/tmp/my-project/node_modules/app-builder-lib/src/targets/fpm.ts:83:13)**  </font>

### 5.1解决办法

修改文件：

> package.json

```js
vim package.json

{
  "name": "buildtest",
  "version": "0.0.1",
  "author": "ningkl <ningkl@thunisoft.com>",
  "description": "An electron-vue project",
  "license": null,
  "homepage": "www.baidu.com",
  "main": "./dist/electron/main.js",
  .....
}
```

> <font color='	DarkRed'> hamagepage   也可以随意填写一个地址 </font>

## 6.报错信息
> • electron-builder version=0.0.0-semantic-release
>
> • loaded configuration file=package.json ("build" field)
>
> • writing effective config file=build/builder-effective-config.yaml
>
> • no native production dependencies
>
> • packaging       platform=linux arch=mips64el electron=4.0.6 appOutDir=build/linux-mips64el-unpacked
>
> <font color='red'> **⨯ cannot resolve https://github.com/electron/electron/releases/download/v4.0.6/electron-v4.0.6-linux-mips64el.zip: status code 404** </font>
>
> github.com/develar/app-builder/pkg/download.(*Downloader).follow
> /home/thtf/huayu/package/app-builder-2.6.0/pkg/download/downloader.go:203

### 6.1 报错说明

> 这里使用的时龙芯平台的electron，所以去下载时，发现没有报了404，解决的思路是，在本地搭一个http服务器，将龙心平台的electron包按照下载的格式打包后，放到 ***/var/www/html/4.0.6***  目录下,修改package.json 文件，指明下载地址

### 6.2 解决办法

修改项目文件
> ./package.json

``` node
vim package.json

 "win": {
      "icon": "build/icons/icon.ico"
    },
    "linux": {
      "icon": "build/icons"
    },
    "electronDownload": {
      "version":"4.0.6",
      "cache":"false",
      "mirror":"http://127.0.0.1/"
            }

=====================================
安装http服务

yum install http*
yum install apache*
```

## 7. 报错信息

> <font color='red'>Error: Exit code: 1. Command failed: /home/dyh/.cache/electron-builder/fpm/fpm-1.9.3-2.3.1-linux-x86/fpm -s dir -t rpm --architecture mips64el --name buildtest --force --after-install /tmp/t-9fju0H/0-after-install --after-remove /tmp/t-9fju0H/1-after-remove --description An electron-vue project --version 0.0.1 --package /home/dyh/tmp/my-project/build/buildtest-0.0.1.mips64el.rpm --maintainer ningkl <ningkl@thunisoft.com> --url http://www.baidu.com --vendor ningkl <ningkl@thunisoft.com> --rpm-compression xzmt --rpm-os linux --depends libnotify --depends libappindicator --depends libXScrnSaver /home/dyh/tmp/my-project/build/linux-mips64el-unpacked/=/opt/buildtest /home/dyh/tmp/my-project/node_modules/app-builder-lib/templates/icons/electron-linux/16x16.png=/usr/share/icons/hicolor/16x16/apps/buildtest.png /home/dyh/tmp/my-project/node_modules/app-builder-lib/templates/icons/electron-linux/24x24.png=/usr/share/icons/hicolor/24x24/apps/buildtest.png /home/dyh/tmp/my-project/node_modules/app-builder-lib/templates/icons/electron-linux/32x32.png=/usr/share/icons/hicolor/32x32/apps/buildtest.png /home/dyh/tmp/my-project/node_modules/app-builder-lib/templates/icons/electron-linux/48x48.png=/usr/share/icons/hicolor/48x48/apps/buildtest.png /home/dyh/tmp/my-project/node_modules/app-builder-lib/templates/icons/electron-linux/64x64.png=/usr/share/icons/hicolor/64x64/apps/buildtest.png /home/dyh/tmp/my-project/node_modules/app-builder-lib/templates/icons/electron-linux/96x96.png=/usr/share/icons/hicolor/96x96/apps/buildtest.png /home/dyh/tmp/my-project/node_modules/app-builder-lib/templates/icons/electron-linux/128x128.png=/usr/share/icons/hicolor/128x128/apps/buildtest.png /home/dyh/tmp/my-project/node_modules/app-builder-lib/templates/icons/electron-linux/256x256.png=/usr/share/icons/hicolor/256x256/apps/buildtest.png /tmp/t-9fju0H/2-buildtest.desktop=/usr/share/applications/buildtest.desktop
>
>/home/dyh/.cache/electron-builder/fpm/fpm-1.9.3-2.3.1-linux-x86/lib/ruby/bin/ruby:行6: /home/dyh/.cache/electron-builder/fpm/fpm-1.9.3-2.3.1-linux-x86/lib/ruby/bin.real/ruby: cannot execute binary file: 可执行文件格式错误
>
>/home/dyh/.cache/electron-builder/fpm/fpm-1.9.3-2.3.1-linux-x86/lib/ruby/bin/ruby:行6: /home/dyh/.cache/electron-builder/fpm/fpm-1.9.3-2.3.1-linux-x86/lib/ruby/bin.real/ruby: 成功
>
>/home/dyh/.cache/electron-builder/fpm/fpm-1.9.3-2.3.1-linux-x86/lib/ruby/bin/ruby:行6: /home/dyh/.cache/electron-builder/fpm/fpm-1.9.3-2.3.1-linux-x86/lib/ruby/bin.real/ruby: cannot execute binary file: 可执行文件格式错误
>
>/home/dyh/.cache/electron-builder/fpm/fpm-1.9.3-2.3.1-linux-x86/lib/ruby/bin/ruby:行6: /home/dyh/.cache/electron-builder/fpm/fpm-1.9.3-2.3.1-linux-x86/lib/ruby/bin.real/ruby: 成功 </font>

解决办法

### 7.1. 安装openssl

```shell
yum install openssl
```

### 7.2 删除 **$HOME/.cache/electron-builder/fpm/fpm-1.9.3-2.3.1-linux-x86/lib/ruby/bin.real/ruby** 文件

``` shell
rm home/dyh/.cache/electron-builder/fpm/fpm-1.9.3-2.3.1-linux-x86/lib/ruby/bin.real/ruby

```

### 7.3将系统中的 **ruby** 可执行文件链接到 **$HOME/.cache/electron-builder/fpm/fpm-1.9.3-2.3.1-linux-x86/lib/ruby/bin.real/**

> 注意
> 由于rbconfig.rb 限制了必须 2.3 版本以上,系统自带的为2.1.5，需要下载源码重新编译

[ruby 2.7.0](https://cache.ruby-lang.org/pub/ruby/2.7/ruby-2.7.0.tar.gz)

[ruby 2.3.1](https://cache.ruby-lang.org/pub/ruby/2.3/ruby-2.3.1.tar.gz) 

[ruby 2.3.8](https://cache.ruby-lang.org/pub/ruby/2.3/ruby-2.3.8.tar.gz)

> 编译ruby

```shell
cd ruby-2.7.0

./configure

make

由于该版本过高，所以使用2.3.0版本，但是使用2.3.0版本，编译使会一直报错，所以放弃使用该办法
```

> 使用rvm安装ruby

```shell
gpg2 --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB

curl -sSL https://get.rvm.io | bash -s stable

source ~/.bashrc

source ~/.bash_profile
--------------------------------------------------------------------------------
修改 RVM 的 Ruby 安装源到 Ruby China 的 [Ruby 镜像服务器](https://cache.ruby-china.com/pub/ruby/)，这样能提高安装速度

echo "ruby_url=https://cache.ruby-china.com/pub/ruby" > ~/.rvm/user/db

没有试过，可以尝试一下
--------------------------------------------------------------------------------
rvm pkg install openssl

rvm reinstall 2.3.0 --with-openssl-dir=$HOME/.rvm/usr

ln -s $HOME/.rvm/rubies/ruby-2.3.0/bin/ruby $HOME/.cache/electron-builder/fpm/fpm-1.9.3-2.3.1-linux-x86/lib/ruby/bin.real/

```
### 7.4 在rvm安装好的ruby环境中安装fpm

``` shell

gem install fpm
rm $HOME/.cache/electron-builder/fpm/fpm-1.9.3-2.3.1-linux-x86/fpm
ln -s $HOME/.rvm/gems/ruby-2.3.0/bin/fpm $HOME/.cache/electron-builder/fpm/fpm-1.9.3-2.3.1-linux-x86/fpm

```
### 7.5 UOS关于这一步的解决办法

> UOS中系统自带的ruby版本为2.5，满足要求，只需要使用gem 安装fpm

``` shell
apt-get install ruby-dev

删除gem 源，使用国内源，解决链接超时问题
gem sources --remove http://rubygems.org/

添加国内源
gem sources -a http://mirrors.aliyun.com/rubygems/

安琥fpm工具
gem install fpm 

```

## 8、报错信息
> • application Linux category is set to default "Utility" reason=linux.category is not set and cannot map from macOS docs=https://electron.build/configuration/configuration#LinuxBuildOptions-category
>
> Error: Exit code: 127. Command failed: /home/dyh/.cache/electron-builder/fpm/fpm-1.9.3-2.3.1-linux-x86/fpm -s dir -t deb --architecture mips64el --name buildtest --force --after-install /tmp/t-2Lte62/0-after-install --after-remove /tmp/t-2Lte62/1-after-remove --description 
 An electron-vue project --version 0.0.1 --package /home/dyh/tmp/my-project/build/buildtest_0.0.1_mips64el.deb --maintainer ningkl <ningkl@thunisoft.com> --url http://www.baidu.com --vendor ningkl <ningkl@thunisoft.com> --deb-compression xz --depends gconf2 --depends gconf-service --depends libnotify4 --depends libappindicator1 --depends libxtst6 --depends libnss3 --depends libxss1 /home/dyh/tmp/my-project/build/linux-mips64el-unpacked/=/opt/buildtest /home/dyh/tmp/my-project/node_modules/app-builder-lib/templates/icons/electron-linux/16x16.png=/usr/share/icons/hicolor/16x16/apps/buildtest.png /home/dyh/tmp/my-project/node_modules/app-builder-lib/templates/icons/electron-linux/24x24.png=/usr/share/icons/hicolor/24x24/apps/buildtest.png /home/dyh/tmp/my-project/node_modules/app-builder-lib/templates/icons/electron-linux/32x32.png=/usr/share/icons/hicolor/32x32/apps/buildtest.png /home/dyh/tmp/my-project/node_modules/app-builder-lib/templates/icons/electron-linux/48x48.png=/usr/share/icons/hicolor/48x48/apps/buildtest.png /home/dyh/tmp/my-project/node_modules/app-builder-lib/templates/icons/electron-linux/64x64.png=/usr/share/icons/hicolor/64x64/apps/buildtest.png /home/dyh/tmp/my-project/node_modules/app-builder-lib/templates/icons/electron-linux/96x96.png=/usr/share/icons/hicolor/96x96/apps/buildtest.png /home/dyh/tmp/my-project/node_modules/app-builder-lib/templates/icons/electron-linux/128x128.png=/usr/share/icons/hicolor/128x128/apps/buildtest.png /home/dyh/tmp/my-project/node_modules/app-builder-lib/templates/icons/electron-linux/256x256.png=/usr/share/icons/hicolor/256x256/apps/buildtest.png /tmp/t-2Lte62/2-buildtest.desktop=/usr/share/applications/buildtest.desktop
>
> <font color="red">/usr/bin/env: “ruby_executable_hooks”: 没有那个文件或目录</font>
>
> <font color="red">/usr/bin/env: “ruby_executable_hooks”: 没有那个文件或目录</font>

### 8.1、解决办法

``` shell

vim $HOME/.bashrc
export PATH="$PATH:$HOME/.rvm/bin:$HOME/.rvm/gems/ruby-2.3.0/bin:$HOME/.rvm/gems/ruby-2.3.0/wrappers/"
添加如下内容

```

打包成功后没法运行，问题解决
===

## 1、没法运行说明

1. 目录说明：

>***/build/linux-mips64el-unpacked*** 这里边存放的就是electron-builder 通过编译后，最终要打包的文件。

2. 问题说明：

>2.1 首先通过与X86平台上打的rpm解压后对比，发现在龙心平台上打的rpm包中缺少对应的demo的可执行文件，也就是electron-builder在打包过程中，没有编译成功demo
>
>2.2  然后通过对比 ***/build/linux-unpacked*** 目录，发现X86平台下的该目录中存放的文件有两类，（1、electron 的文件；2、编译的demo的可执行文件），而龙芯平台中 ***/build/linux-mips64el-unpacked*** 目录下多了一层目录，并且进去之后发现里边只有electron的文件，并且还缺少一个名为 ***swiftshader*** 的目录。

## 2、 解决办法：

1. 先解压龙芯平台的rpm包，然后参照X86平台上该 ***/build/linux-unpacked*** 目录下目录结构，重新放置文件。

2. 然后将该目录下的文件打成名为 ***electron-4.0.6-linux-mips64el.zip*** 的压缩包，然后将其拷贝到 ***/var/www/html/4.0.6/*** 目录下

3. <font color='red'> 注意: 打包时，不要讲外层包裹文件的那层目录打进去，不然就会在 ***/build/linux-mips64el-unpacked*** 目录下多一层目录，使编译demo失败。</font>

4. 操作步骤

```shell
rpm2cpio electron-4.0.6-nd7.mips64el.rpm |cpio -div
mkdir tmp
cp ./usr/bin/* ./tmp
cp ./usr/lib64/electron-v4.0.6/*.so ./tmp
cp ./usr/lib64/electron-v4.0.6/*.bin ./tmp
cp ./usr/lib64/electron-v4.0.6/LICEN* ./tmp
cp ./usr/lib64/electron-v4.0.6/chrome*.pak ./tmp
cp ./usr/lib64/electron-v4.0.6/resources.pak ./tmp
cp ./usr/lib64/electron-v4.0.6/version ./tmp
cp -r ./usr/lib64/electron-v4.0.6/locales ./tmp
cp -r ./usr/lib64/electron-v4.0.6/resources ./tmp
rm -rf ./tmp/locales/*.info
rm -rf ./tmp/resources/inspector
cd tmp
mkdir swiftshader
cp LICEN*.so ./swiftshader
zip -r electron-4.0.6-linux-mips64el.zip *
cp electron-4.0.6-linux-mips64el.zip /var/www/html/4.0.6/
```

UOS系统上打的deb包，安装问题
===

## 1、报错信息

``` shell

loongson@loongson-PC:~/tmp/my-project/build$ sudo dpkg -i buildtest_0.0.1_mips64el.deb

```

> tar: 归档被压缩。使用 -J 选项
>
> tar: Error is not recoverable: exiting now
>
> dpkg-deb: 错误: tar 子进程返回错误状态 2
>
> dpkg: 处理归档 buildtest_0.0.1_mips64el.deb (--install)时出错：
>
> dpkg-deb --control 子进程返回错误状态 2
>
> 在处理时有错误发生：
>
> buildtest_0.0.1_mips64el.deb

## 2、排查问题

``` shell
 sudo apt install ./buildtest_0.0.1_mips64el.deb
 
这里的./ 必须携带

```
> 正在读取软件包列表... 有错误！
>
> E: 子进程 Popen 返回了一个错误号 (2)
>
> E: Encountered a section with no Package: header
>
> E: Problem with MergeList /home/loongson/tmp/my-project/build/buildtest_0.0.1_mips64el.deb
>
>E: 无法解析或打开软件包的列表或是状态文件。

通过使用上述办法，确认打的deb包有问题，使用 **ar -x** 命令解压后，发现有两个归档文件（1、control.tar.gz, data.tar.xz ）与X86平台做对比发现X86平台上也是这样的打包方式，使用 **tar -zxvf control.tar.gz** 解压包时，发现解压不了，猜测是打包时，在压缩包时出现了问题，但我没有办法去确认，最后发现在 ***./node_modules/app-builder-lib/out/targets/fpm.js*** 中有一个使用fpm 打包时，进行归档压缩时的，格式选项，尝试修改为 **gz** 格式，重新打包后，重新使用命令安装后，可以成功安装，这个问题终于解决。

## 3、解决办法

### 3.1、修改文件路径

> ./node_modules/app-builder-lib/out/targets/fpm.js 

```shell
vim ./node_modules/app-builder-lib/out/targets/fpm.js +268

if (target === "deb") {
      args.push("--deb-compression", compression || "gz");
      (0, _builderUtil().use)(options.priority, it => args.push("--deb-priority", it));
    } 

```


UOS系统，安装完后，崩溃问题
===

## 1、在uos上,安装好，打好的deb包后，启动程序会出现崩溃

### 1.1、问题分析

> 通过报错信息，发现是有共享库导致的，通过验证，发现在UOS系统上第一次安装都会出现该问题，在排查问题的过程中，发现移动一下导致报错的共享库的位置，在移动会安装目录，在启动，崩溃问题就小时，该问题不好排查，所以目前只能先手动修改一下。

### 1.2、解决办法

···shell
cd /opt/opt/buildtest/buildtest/
mv libnss3.so libnss3.so_bak
mv libnssutil3.so libnssutil3.so_bak
mv libnss3.so_bak libnss3.so
mv libnssutil3.so_bak libnssutil3.so

```
